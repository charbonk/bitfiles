#
# inspire de https://github.com/mathiasbynens/dotfiles
#

# Load the shell dotfiles, and then some:
# * ~/.path can be used to extend `$PATH`.
# * ~/.extra can be used for other settings you don’t want to commit.
for file in ~/.{path,bash_prompt,exports,aliases,functions,osx,linux,extra}; do
	[ -r "$file" ] && [ -f "$file" ] && source "$file"
done
unset file

export PATH=/usr/local/bin:$PATH:~/bin:~/git/git-lexflow/git-lexflow-core/src/main/bash

source ~/.git-completion
source ~/.maven_bash_completion.bash

export MAVEN_OPTS="-Xmx1524m"

export PATH="$HOME/.rbenv/bin:$PATH"

eval "$(rbenv init -)"
eval "$(pyenv init -)"

